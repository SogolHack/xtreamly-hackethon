using System.ComponentModel.DataAnnotations;

namespace Xtreamly.Models;

public class PaginationDto
{
    [Required(ErrorMessage = "Skip is required in pagination")]
    public int Skip { get; set; }
    
    [Required(ErrorMessage = "Count is required in pagination")]
    public int Count { get; set; }
}
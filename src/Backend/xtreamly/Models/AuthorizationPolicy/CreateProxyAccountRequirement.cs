using Microsoft.AspNetCore.Authorization;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.AuthorizationPolicy;

public class CreateProxyAccountRequirement: IAuthorizationRequirement
{
    
}

public class CreateProxyAccountRequirementHandler : AuthorizationHandler<CreateProxyAccountRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CreateProxyAccountRequirement requirement)
    {
        if (!context.User.HasClaim(c => c.Type == "capabilities")) return Task.CompletedTask;

        var capabilitiesJson = context.User.Claims.SingleOrDefault(clainm => clainm.Type == "capabilities").Value;
        var capabilities = System.Text.Json.JsonSerializer.Deserialize<List<TokenCapabilities>>(capabilitiesJson);
        if (capabilities != null && capabilities.Contains(TokenCapabilities.MANAGE_PROXY_ACCOUNT))
        {
            context.Succeed(requirement);
        }
        else
        {
            context.Fail(new AuthorizationFailureReason(this, "token does not have access for the required action"));
        }

        return Task.CompletedTask;
    }
}
using Microsoft.AspNetCore.Authorization;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.AuthorizationPolicy;

public class FetchListOfProxyRequirement :  IAuthorizationRequirement
{
    
}
public class FetchListOfProxyRequirementHandler : AuthorizationHandler<FetchListOfProxyRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FetchListOfProxyRequirement requirement)
    {
        if (!context.User.HasClaim(c => c.Type == "capabilities")) return Task.CompletedTask;

        var capabilitiesJson = context.User.Claims.SingleOrDefault(clainm => clainm.Type == "capabilities").Value;
        var capabilities = System.Text.Json.JsonSerializer.Deserialize<List<TokenCapabilities>>(capabilitiesJson);
        if (capabilities != null && capabilities.Contains(TokenCapabilities.FETCH_LIST_OF_PROXY))
        {
            context.Succeed(requirement);
        }
        else
        {
            context.Fail(new AuthorizationFailureReason(this, "token does not have access for the required action"));
        }

        return Task.CompletedTask;
    }
}
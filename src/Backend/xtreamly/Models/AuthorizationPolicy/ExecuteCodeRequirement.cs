using Microsoft.AspNetCore.Authorization;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.AuthorizationPolicy;

public class ExecuteCodeRequirement: IAuthorizationRequirement
{
    
}

public class ExecuteCodeRequirementHandler : AuthorizationHandler<ExecuteCodeRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ExecuteCodeRequirement requirement)
    {
        if (!context.User.HasClaim(c => c.Type == "capabilities")) return Task.CompletedTask;

        var capabilitiesJson = context.User.Claims.SingleOrDefault(clainm => clainm.Type == "capabilities").Value;
        var capabilities = System.Text.Json.JsonSerializer.Deserialize<List<TokenCapabilities>>(capabilitiesJson);
        if (capabilities != null && capabilities.Contains(TokenCapabilities.EXECUTE_CODE))
        {
            context.Succeed(requirement);
        }
        else
        {
            context.Fail(new AuthorizationFailureReason(this, "token does not have access for the required action"));
        }

        return Task.CompletedTask;
    }
}
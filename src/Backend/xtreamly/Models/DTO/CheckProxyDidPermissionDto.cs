using System.ComponentModel.DataAnnotations;

namespace Xtreamly.Models.Dto;

public class DidPermissionDto
{
    [Required]
    public string DappSecret { get; set; }
    
    [Required]
    public string Did { get; set; }
    
    [Required]
    public string ProxyId { get; set; }
}

using System.ComponentModel.DataAnnotations;

namespace Xtreamly.Models.Dto;

public class ProxyAccountDto
{
    [Required (ErrorMessage = "name is needed for creating proxy account")]
    
    public string Name { get; set; }
}
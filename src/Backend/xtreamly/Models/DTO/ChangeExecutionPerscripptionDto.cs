using Microsoft.Build.Framework;

namespace Xtreamly.Models.Dto;

public class ChangeExecutionPerscripptionDto
{

    [Required]
    public string ExecutionPerscriptionId { get; set; }
    
    [Required]
    public bool NewStatus { get; set; }
}
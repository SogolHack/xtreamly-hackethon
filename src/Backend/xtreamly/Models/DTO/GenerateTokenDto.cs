using System.ComponentModel.DataAnnotations;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.Dto;

public class GenerateTokenDto
{
    [Required(ErrorMessage = "you need to provide capabilities")]
    public List<TokenCapabilities> TokenCapabilitiesList { get; set; }
    
    [Required(ErrorMessage = "you need to provide expire time")]
    public DateTime ExpireTime { get; set; }
    
    public bool IsTimeValid()
    {
        return ExpireTime >= DateTime.UtcNow ;
    }
}
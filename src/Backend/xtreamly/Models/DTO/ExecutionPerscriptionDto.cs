using System.ComponentModel.DataAnnotations;

namespace Xtreamly.Models.Dto;

public class ExecutionPerscriptionDto
{
    [Required]
    public string AppletId { get; set; }
    
    [Required]
    public string Metadata { get; set; }
    
    [Required]
    public string ProxyAccountId { get; set; }
    
    [Required]
    public string Cron { get; set; }
    
    [Required]
    public bool Active { get; set; }

}
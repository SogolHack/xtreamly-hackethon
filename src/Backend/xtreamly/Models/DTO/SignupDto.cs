using System.ComponentModel.DataAnnotations;

namespace Xtreamly.Models.Dto;

public class SignupDto
{
    [Required(AllowEmptyStrings = false, ErrorMessage = "email is required")]
    public string Email { get; set; }
    
    [Required(AllowEmptyStrings = false, ErrorMessage = "password is required")]
    public string Password { get; set; }
    
}
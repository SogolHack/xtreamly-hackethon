
using System.ComponentModel.DataAnnotations;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.Dto;

public class AppletDto
{
    [Required(AllowEmptyStrings = false, ErrorMessage = "Metadata can not be null")]
    public string Metadata { get; set; }
    
    [Required(AllowEmptyStrings = false, ErrorMessage = "Script can not be null")]
    public string Script { get; set; }

    [Required(AllowEmptyStrings = false, ErrorMessage = "AppletVisibility can not be null")]
    public AppletVisibility AppletVisibility { get; set; }
    

    
}
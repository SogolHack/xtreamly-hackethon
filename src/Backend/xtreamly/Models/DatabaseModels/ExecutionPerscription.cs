using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xtreamly.Models.DatabaseModels;

public class ExecutionPerscription
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public string Id { get; set; }
    
    public string Metadata { get; set; }
    public Applet Applet { get; set; }
    
    public ProxyAccount ProxyAccount { get; set; }
    
    public string Cron { get; set; }
    
    public bool Active { get; set; }
    
    
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.DatabaseModels;

public record class Applet
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public string Id { get; set; }
    
    public string Metadata { get; set; }
    
    public Script Script { get; set; }
    
    public XtreamlyUser Owner { get; set; }
    
    public AppletType AppletType { get; set; }
    
    public double AppletCompabilityVersion { get; set; }
    
    
    [DataType(DataType.DateTime)]
    
    public DateTime CreationDate { get; set; }
    
    public AppletVisibility Visibility { get; set; }
}
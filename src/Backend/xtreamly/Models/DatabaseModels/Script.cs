using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xtreamly.Models.DatabaseModels;

public record class Script
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public string Id { get; set; }
    
    public double ScriptCompabilityVersion { get; set; }
    
    public string ScriptText { get; set; }
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Xtreamly.Models.Enums;

namespace Xtreamly.Models.DatabaseModels;

public class ProxyAccount
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    
    public string Id { get; set; }
    
    public string Name { get; set; }

    public string Mnemonica { get; set; }
    
    [DataType(DataType.DateTime)]
    
    public DateTime CreationDate { get; set; }

    public XtreamlyUser Owner { get; set; }
    
    public bool Active { get; set; }
    
    public ExecutionStatus ExecutionStatus { get; set; }
}
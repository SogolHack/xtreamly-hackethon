namespace Xtreamly.Models.Enums;

public enum SupportedChains
{
    EVM,
    SOLANA,
    BITCOIN
}
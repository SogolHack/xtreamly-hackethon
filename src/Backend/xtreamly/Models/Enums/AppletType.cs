namespace Xtreamly.Models.Enums;

public enum AppletType
{
    PERMISSION,
    GENERAL
}
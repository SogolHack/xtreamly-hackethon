namespace Xtreamly.Models.Enums;

public enum AppletVisibility
{
    PUBLIC,
    PRIVATE
}
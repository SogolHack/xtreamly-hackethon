using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Models;

public class ApplicationDbContext : IdentityDbContext
{
    
    public virtual DbSet<ExecutionReport>   ExecutionReports { get; set; }
    public virtual DbSet<ExecutionPerscription> ExecutionPerscriptions { get; set; }
    public virtual DbSet<XtreamlyUser> XtreamlyUsers { get; set; }

    public virtual DbSet<Applet> Applets { get; set; }
    
    public virtual DbSet<ProxyAccount> ProxyAccounts { get; set; }
    
    public virtual DbSet<Script> Scripts { get; set; }
    
    
    public ApplicationDbContext(DbContextOptions option) : base(option)
    {
    }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }
}
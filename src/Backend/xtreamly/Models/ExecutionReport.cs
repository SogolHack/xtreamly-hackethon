using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Models;

public class ExecutionReport
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public string Id { get; set; }
    
    
    [JsonIgnore]
    public string ExecutionPerscriptionId { get; set; }
    
    public DateTime DateTime { get; set; }
    
    public string Message { get; set; }
    
    public string MetaData { get; set; }
}
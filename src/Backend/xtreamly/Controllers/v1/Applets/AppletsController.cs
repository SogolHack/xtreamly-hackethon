using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xtreamly.Models;
using Xtreamly.Models.Dto;
using Xtreamly.Services.ApplicatinServices.Commands;
using Xtreamly.Services.ApplicatinServices.Queryies;

namespace Xtreamly.Controllers.v1.Applets
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AppletsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AppletsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize(Roles = UserRoles.General, Policy = "PublishAppletPolicy")]
        [HttpPost]
        public async Task<IActionResult> PublishNewApplet([FromBody] AppletDto appletDto)
        {
            try
            {
                var result = await _mediator.Send(new PublishNewAppletCommand(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(),appletDto));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpGet]
        public async Task<IActionResult> GetAllMyExecutionPerscriptions([FromQuery] int skip , [FromQuery] int count)
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };
                
                
                var result = await _mediator.Send(new GetAllExecutionPerscriptionsForUserQuery(pg,
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpGet]
        public async Task<IActionResult> GetAllMyExecutionPerscriptionsByProxyId([FromQuery] int skip , [FromQuery] int count, [FromQuery] string proxyId)
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };
                
                
                var result = await _mediator.Send(new GetAllExecutionPerscriptionsForUserByProxyIdQuery(pg,
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), proxyId));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }





        [Authorize(Roles = UserRoles.General, Policy = "ExecuteCodePolicy")]
        [HttpPost]
        public async Task<IActionResult> CreateExecutionPerscription([FromBody] ExecutionPerscriptionDto executionPerscriptionDto)
        {
            try
            {
                var result = await _mediator.Send(new CreateExecutionPerscriptionCommand(executionPerscriptionDto,
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        [Authorize(Roles = UserRoles.General)]
        [HttpGet]
        public async Task<IActionResult> GetAllApplets([FromQuery] int skip , [FromQuery] int count)
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };
                var result = await _mediator.Send(new GetAllAppletsQuery(pg));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        
        [Authorize(Roles = UserRoles.General)]
        [HttpGet]
        public async Task<IActionResult> GetAllMyApplets([FromQuery] int skip , [FromQuery] int count)
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };
                var result = await _mediator.Send(new GetAllAppletsForUserQuery(pg, User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        
        [Authorize(Roles = UserRoles.General, Policy = "ExecuteCodePolicy")]
        [HttpGet]
        public async Task<IActionResult> GetLogsForExecutionPerscription([FromQuery] string executionPerscriptionId, [FromQuery] int skip , [FromQuery] int count) 
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };

                var result = await _mediator.Send(new GetLogsForExecutionPerscriptionQuery(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), executionPerscriptionId , pg));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpPatch]
        public async Task<IActionResult> ChangeExecutionPerscripptionStatus([FromBody] ChangeExecutionPerscripptionDto changeExecutionPerscripptionDto)
        {
            try
            {
                var _changeExecutionPerscripptionDto = new ChangeExecutionPerscripptionDto()
                {
                    NewStatus = changeExecutionPerscripptionDto.NewStatus,
                    ExecutionPerscriptionId = changeExecutionPerscripptionDto.ExecutionPerscriptionId
                };
                
                
                var result = await _mediator.Send(new ChangeExecutionPerscripptionStatusCommand(_changeExecutionPerscripptionDto,
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        
        
        
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xtreamly.Models;
using Xtreamly.Services.ApplicatinServices.Queryies;

namespace Xtreamly.Controllers.v1.User
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [Authorize(Roles = UserRoles.General)]
        [HttpGet]
        public async Task<IActionResult> GetMyProfile()
        {
            try
            {
                var result = await _mediator.Send(new GetProfileForUserQuery(User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                                                                        throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }


    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xtreamly.Models;
using Xtreamly.Services.ApplicatinServices.Queryies;

namespace Xtreamly.Controllers.v1.AI
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class CodeGenerationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CodeGenerationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize(Roles = UserRoles.General)]
        [HttpPost]
        public async Task<IActionResult> GeneratePermissionCode([FromBody] string text)
        {
            try
            {
                var result = await _mediator.Send(new GeneratePermissionCodeByOpenAIQuery(text));
                return result.IsT0 ? Ok(result.AsT0) : StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
            
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xtreamly.Models;
using Xtreamly.Models.Dto;
using Xtreamly.Models.Enums;
using Xtreamly.Services.ApplicatinServices.Commands;
using Xtreamly.Services.ApplicatinServices.Queryies;

namespace Xtreamly.Controllers.v1.ProxyAccount
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class ProxyAccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProxyAccountController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [Authorize(Roles = UserRoles.General, Policy = "FetchListOfProxyPolicy") ]
        [HttpGet]
        public async Task<IActionResult> GetMyProxyAccounts([FromQuery] int skip, [FromQuery] int count)
        {
            try
            {
                var pg = new PaginationDto()
                {
                    Count = count,
                    Skip = skip
                };
                var result = await _mediator.Send(new GetProxyAccountForUserQuery(pg, User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException()));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpPost]
        public async Task<IActionResult> CreateNewProxyAccount([FromBody] ProxyAccountDto proxyAccountDto)
        {
            try
            {
                var result = await _mediator.Send(new CreateNewProxyAccountCommand(User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), proxyAccountDto));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        
        
        [Authorize(Roles = UserRoles.General, Policy = "GetProxyAccountAddressPolicy")]
        [HttpGet]
        public async Task<IActionResult> GetAddressForProxyAccount([FromQuery] SupportedChains chain, [FromQuery] string ProxyAccountId)
        {
            try
            {

                var result = chain switch
                {
                    SupportedChains.EVM => await _mediator.Send(new GetEthereumAddressForProxyAccountQuery(
                        ProxyAccountId,
                        User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                        throw new InvalidOperationException())),
                    SupportedChains.SOLANA => await _mediator.Send(new GetSolanaAddressForProxyQuery(ProxyAccountId,
                        User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                        throw new InvalidOperationException())),
                    SupportedChains.BITCOIN => await _mediator.Send(new GetBitcoinAddressForProxyQuery(ProxyAccountId,
                        User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                        throw new InvalidOperationException())),
                };
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        
        [Authorize(Roles = UserRoles.General, Policy = "GenerateOtherTokensPolicy")]
        [HttpPost]
        public async Task<IActionResult> GenerateTokenForMyAccount([FromBody] GenerateTokenDto info)
        {
            try
            {
                if (!info.IsTimeValid())
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "expire time can not be in the past");
                }

                var result = await _mediator.Send(new GenerateTokenForAccountQuery(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), info.TokenCapabilitiesList,info.ExpireTime));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpPatch]
        public async Task<IActionResult> ChangeProxyAccountActivision([FromQuery] string proxyId , [FromQuery] bool active )
        {
            try
            {

                var result = await _mediator.Send(new ChangeProxyAccountActivisionCommand(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), proxyId,active));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        
        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpPut]
        public async Task<IActionResult> AddOrUpdateDynamicPermission([FromBody] AppletDto appletDto )
        {
            try
            {

                var result = await _mediator.Send(new AddOrUpdateDynamicPermisionCommand(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), appletDto));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        
        
        [Authorize(Roles = UserRoles.General)]
        [HttpGet]
        public async Task<IActionResult> GetDid([FromQuery] string dappSecret,  [FromQuery] string did, [FromQuery] string proxyId)
        {
            try
            {

                var result = await _mediator.Send(new GetDidQuery(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), did, dappSecret ,proxyId ));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpGet]
        public async Task<IActionResult> GetMyPermissionApplet()
        {
            try
            {

                var result = await _mediator.Send(new GetPermissionAppletForUserQuery(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException() ));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        [Authorize(Roles = UserRoles.General, Policy = "ManageProxyAccountPolicy")]
        [HttpPatch]
        public async Task<IActionResult> ChangeProxyAccountExecutionStatus([FromQuery] string proxyId  ,[FromBody] ExecutionStatus executionStatus)
        {
            try
            {

                var result = await _mediator.Send(new ChangeProxyAccountExecutionStatusCommand(
                    User.Claims.SingleOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value ??
                    throw new InvalidOperationException(), proxyId ,  executionStatus));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}

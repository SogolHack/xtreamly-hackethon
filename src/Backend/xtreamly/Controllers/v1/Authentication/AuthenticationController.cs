using MediatR;
using Microsoft.AspNetCore.Mvc;
using Xtreamly.Models.Dto;
using Xtreamly.Services.ApplicatinServices.Commands;
using Xtreamly.Services.ApplicatinServices.Queryies;

namespace Xtreamly.Controllers.v1.Authentication
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        
        
        /// <summary>
        /// sign up a new user in Xtraemly, refer to SignupDto class for mor details
        /// </summary>
        /// <param name="signupDto">the sign up info</param>
        /// <returns>true if succeeded, otherwise would return 400 bad request with message </returns>
        [HttpPost]
        public async Task<IActionResult> Signup([FromBody] SignupDto signupDto)
        {
            try
            {
                var result = await _mediator.Send(new SignupCommand(signupDto));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        
        /// <summary>
        /// sign in to Xtreamly, the resulting user will have all token capabilities possible
        /// </summary>
        /// <param name="signupDto"></param>
        /// <returns>jwt token</returns>
        
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginDto signupDto)
        {
            try
            {
                var result = await _mediator.Send(new LoginQuery(signupDto));
                if (result.IsT0)
                {
                    return Ok(
                        new
                        {
                            data = result.AsT0
                        });
                }

                return StatusCode(StatusCodes.Status400BadRequest, result.AsT1.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
    }
}

namespace Xtreamly.Services.FunctionalityProviders.AirstackApiClient;

using RestSharp;

public class AirstackApiClient
{
    private readonly RestClient _client;
    private readonly string _bearerToken;
    private readonly string _origin;

    public AirstackApiClient(string baseUrl, string bearerToken, string origin)
    {
        _client = new RestClient(baseUrl);
        _bearerToken = bearerToken;
        _origin = origin;
    }

    public static string MakeAirstackRequest(string baseUrl, string bearerToken, string origin, string question, decimal risckFactor)
    {
        var client = new RestClient(baseUrl);
        var request = new RestRequest("/api/web/generate-graphql", Method.POST);

        request.AddHeader("authority", "ai.airstack.xyz");
        request.AddHeader("accept", "*/*");
        request.AddHeader("accept-language", "en-US,en;q=0.9,fa-IR;q=0.8,fa;q=0.7,af;q=0.6");
        request.AddHeader("authorization", "Bearer " + bearerToken);
        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("content-type", "application/json");
        request.AddHeader("dnt", "1");
        request.AddHeader("origin", origin);
        request.AddHeader("pragma", "no-cache");
        request.AddHeader("referer", origin + "/");
        request.AddHeader("sec-ch-ua", "\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\", \"Google Chrome\";v=\"114\"");
        request.AddHeader("sec-ch-ua-mobile", "?0");
        request.AddHeader("sec-ch-ua-platform", "\"macOS\"");
        request.AddHeader("sec-fetch-dest", "empty");
        request.AddHeader("sec-fetch-mode", "cors");
        request.AddHeader("sec-fetch-site", "same-site");
        request.AddHeader("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36");
        return "0x111111111117dc0aa78b770fa6a738034120c302";
        var requestData = new { question };
        request.AddJsonBody(requestData);

        var response = client.Execute(request);

        if (response.IsSuccessful)
        {
          
            return response.Content;
        }
        else
        {
            // Handle the error here
            // You can throw an exception or return an error message as needed.
            return null;
        }
    }
}
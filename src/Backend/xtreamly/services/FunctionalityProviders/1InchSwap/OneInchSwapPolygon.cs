using NBitcoin;
using Nethereum.HdWallet;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Newtonsoft.Json;
using RestSharp;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.FunctionalityProviders._1InchSwap;


public class TokenInfo
{
    [JsonProperty("symbol")]
    public string Symbol { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("address")]
    public string Address { get; set; }

    [JsonProperty("decimals")]
    public int Decimals { get; set; }

    [JsonProperty("logoURI")]
    public string LogoURI { get; set; }
}

public class TransactionInfo
{
    [JsonProperty("from")]
    public string From { get; set; }

    [JsonProperty("to")]
    public string To { get; set; }

    [JsonProperty("data")]
    public string Data { get; set; }

    [JsonProperty("value")]
    public string Value { get; set; }

    [JsonProperty("gasPrice")]
    public string GasPrice { get; set; }

    [JsonProperty("gas")]
    public int Gas { get; set; }
}

public class SwapResponse
{
    [JsonProperty("fromToken")]
    public TokenInfo FromToken { get; set; }

    [JsonProperty("toToken")]
    public TokenInfo ToToken { get; set; }

    [JsonProperty("toAmount")]
    public string ToAmount { get; set; }

    [JsonProperty("protocols")]
    public List<string> Protocols { get; set; }

    [JsonProperty("tx")]
    public TransactionInfo Transaction { get; set; }
}
public class TransactionHashResponse
{
    [JsonProperty("transactionHash")]
    public string TransactionHash { get; set; }
}

public class DelegationTransactionResponse
{
    [JsonProperty("data")]
    public string Data { get; set; }

    [JsonProperty("gasPrice")]
    public string GasPrice { get; set; }

    [JsonProperty("to")]
    public string To { get; set; }

    [JsonProperty("value")]
    public string Value { get; set; }
}

public class OneInchSwapPolygon
{
    private readonly ProxyAccount _proxyAccount;

    public OneInchSwapPolygon(ProxyAccount proxyAccount)
    {
        _proxyAccount = proxyAccount;
    }

    private const string ONE_INCH_API = "Bc1Fe022vs43KHEvmUBQMfzdUzCh1RFw";
    public bool ServiceHealthCheck()
    {
        // Define the API base URL and endpoint
        string baseUrl = "https://api.1inch.dev";
        string endpoint = "/swap/v5.2/1/healthcheck";

        // Create the RestClient
        var client = new RestClient(baseUrl);

        // Create the RestRequest with the GET method
        var request = new RestRequest(endpoint, Method.GET);

        // Set the required headers
        request.AddHeader("accept", "*/*");
        request.AddHeader("Authorization", "Bearer Bc1Fe022vs43KHEvmUBQMfzdUzCh1RFw");

        // Execute the request and get the response
        var response = client.Execute(request);

        // Check if the request was successful (status code 200)
        return response.StatusCode == System.Net.HttpStatusCode.OK;
    }
    public DelegationTransactionResponse? CreateDelegationTransaction(string tokenAddress, ulong amount)
    {
        // Define the API base URL and endpoint
        var baseUrl = "https://api.1inch.dev";
        var endpoint = "/swap/v5.2/1/approve/transaction";

        // Create the RestClient
        var client = new RestClient(baseUrl);

        // Create the RestRequest with the GET method and set query parameters
        var request = new RestRequest(endpoint, Method.GET);
        request.AddParameter("tokenAddress", tokenAddress, ParameterType.QueryString);
        request.AddParameter("amount", amount, ParameterType.QueryString);

        // Set the required headers
        request.AddHeader("accept", "application/json");
        request.AddHeader("Authorization", "Bearer Bc1Fe022vs43KHEvmUBQMfzdUzCh1RFw");

        // Execute the request and get the response
        var response = client.Execute(request);

        // Deserialize the JSON response to the DelegationTransactionResponse class
        return JsonConvert.DeserializeObject<DelegationTransactionResponse>(response.Content);
    }

    public async Task<string> SignTransactionFromDelegationTransactionResponse(
        DelegationTransactionResponse delegationTransactionResponse)
    {
        // Example DelegationTransactionResponse (replace this with the actual response from the API)
        // Replace this with your actual private key

        // Polygon Mainnet RPC URL
        var rpcUrl = "https://polygon-rpc.com";

 

        // Get the account address from the private key
        
        var publicKey = (new Wallet(new Mnemonic(_proxyAccount.Mnemonica).DeriveSeed())).GetAccount(0).PublicKey;
        var senderAddress = publicKey;

        
        // Create a Web3 object connected to the Polygon Mainnet using the RPC URL
        var privatekey = (new Wallet(new Mnemonic(_proxyAccount.Mnemonica).DeriveSeed())).GetAccount(0).PrivateKey;
        var account = new Account(privatekey, 137);
        var web3 = new Web3 (account, rpcUrl);
        
        
        // Get the next available nonce for the sender's address
        var nonce = await web3.Eth.Transactions.GetTransactionCount.SendRequestAsync(senderAddress);

        // Prepare the transaction parameters
        var transactionInput = new TransactionInput
        {
            From = senderAddress,
            To = delegationTransactionResponse.To,
            Value = new HexBigInteger(delegationTransactionResponse.Value),
            GasPrice = new HexBigInteger(delegationTransactionResponse.GasPrice),
            Data = delegationTransactionResponse.Data,
            Nonce = new HexBigInteger(nonce),
        };

        // Sign the transaction with the private key
        var signedTransaction = await web3.Eth.TransactionManager.SignTransactionAsync(transactionInput);

        // Encode the transaction to a raw byte array
        return signedTransaction;
    } 
    
    public TransactionHashResponse? SubmitRawTransaction(string rawTransaction)
    {
        // Define the API base URL and endpoint
        var baseUrl = "https://api.1inch.dev";
        var endpoint = "/tx-gateway/v1.1/1/broadcast";

        // Create the RestClient
        var client = new RestClient(baseUrl);

        // Create the RestRequest with the POST method and set the JSON payload
        var request = new RestRequest(endpoint, Method.POST);
        request.AddHeader("accept", "application/json");
        request.AddHeader("Content-Type", "application/json");
        request.AddJsonBody(new { rawTransaction });

        // Execute the request and get the response
        var response = client.Execute(request);

        // Check if the request was successful and deserialize the response
        if (response.IsSuccessful)
        {
            // Deserialize the JSON response to the TransactionHashResponse class
            return JsonConvert.DeserializeObject<TransactionHashResponse>(response.Content);
        }

        return null;
    }
    
    
    public SwapResponse? GetSwapInfo(string srcTokenAddress, string dstTokenAddress, ulong amount, string fromAddress, int slippage)
    {
        // Define the API base URL and endpoint with the provided parameters
        var baseUrl = "https://api.1inch.dev";
        var endpoint = $"/swap/v5.2/1/swap?src={srcTokenAddress}&dst={dstTokenAddress}&amount={amount}&from={fromAddress}&slippage={slippage}";

        // Create the RestClient
        var client = new RestClient(baseUrl);

        // Create the RestRequest with the GET method and set the accept header
        var request = new RestRequest(endpoint, Method.GET);
        request.AddHeader("accept", "application/json");

        // Execute the request and get the response
        var response = client.Execute(request);

        // Check if the request was successful and deserialize the response
        if (response.IsSuccessful)
        {
            // Deserialize the JSON response to the SwapResponse class
            return JsonConvert.DeserializeObject<SwapResponse>(response.Content);
        }
        else
        {
            return null;
        }
    }
}
using Humanizer;
using NLua;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Services.FunctionalityProviders._1InchSwap;

namespace Xtreamly.Services.FunctionalityProviders;

public class GeneralAppletFunctionalityProvider
{
    private readonly ILogger<GeneralAppletFunctionalityProvider> _logger;
    private  Lua _lua;
    private readonly ApplicationDbContext _applicationDbContext;
    private  ExecutionPerscription _executionPerscription;
    

    public GeneralAppletFunctionalityProvider(ApplicationDbContext applicationDbContext, ILogger<GeneralAppletFunctionalityProvider> logger)
    {
        _applicationDbContext = applicationDbContext;
        _logger = logger;
    }
    
    internal void SetApplet(ref ExecutionPerscription executionPerscription)
    {
        _executionPerscription = executionPerscription;
    }

    internal void SetLua(ref Lua lua)
    {
        _lua = lua;
    }

    public long GetMemory()
    {
        if (_lua == null)
        {
            throw new Exception("lua state is not set");
        }

        return _lua.CurrentAvailableMemory;
    }
    
    public string GetScriptSource()
    {
        if (_executionPerscription == null)
        {
            throw new Exception("_applet is not set");
        }

        return _executionPerscription.Applet.Script.ScriptText;
    }
    public string GetProxyId()
    {
        if (_executionPerscription == null)
        {
            throw new Exception("_applet is not set");
        }

        return _executionPerscription.ProxyAccount.Id;
    }
    public string GetAppletId()
    {
        if (_executionPerscription == null)
        {
            throw new Exception("_applet is not set");
        }

        return _executionPerscription.Applet.Id;
    }
    
    public long GetCpuInstructionCount()
    {
        if (_lua == null)
        {
            throw new Exception("lua state is not set");
        }

        return _lua.CurrentInstructionCount;
    }
    
    public bool Log(string message, string metadata)
    {
         using var transaction =  _applicationDbContext.Database.BeginTransaction();
        try
        {
            _applicationDbContext.ExecutionReports.Add(new ExecutionReport()
            {
                //todo fix this
                ExecutionPerscriptionId = _executionPerscription.Id,
                Message = message.Truncate(50),
                DateTime = DateTime.UtcNow,
                MetaData = metadata.Truncate(250)
            });
            
             _applicationDbContext.SaveChanges();
             transaction.Commit();

            return true;
        }

        catch (Exception e)
        {
            _logger.LogError("lua logging error : {Error}" ,e.Message);
            return false;
        }
    }

    
    public string GetTokenAccordingToCondition(decimal riskfactor, string query )
    {
        var baseUrl = "https://ai.airstack.xyz";
        var bearerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOiJjbGtheWNpdjkwMHk5bWIwODl0cm5xbGdwIiwiaXNzIjoicHJpdnkuaW8iLCJpYXQiOjE2ODk5NDQ1NjAsImF1ZCI6ImNsZzZseDV3MDAwMGZqdjA4eGV6aWgwbnEiLCJzdWIiOiJkaWQ6cHJpdnk6Y2xrYXljaXcxMDB5Ym1iMDhoejB5a3FncyIsImV4cCI6MTY4OTk0ODE2MH0._qppHE7e2LI0gomQroeu9xxbBzMEacnHKg6wrcyNfXg0heGGHa30wSl5g_CGDhb6Sdh1CMSqlVNWoScEnFyL7A";
        var origin = "https://app.airstack.xyz";

        var question = "query";
        var bestToken = AirstackApiClient.AirstackApiClient.MakeAirstackRequest(baseUrl, bearerToken, origin, question, riskfactor);
        return bestToken;
    }
    public string GetBestTokenForSwapOnPolygon(decimal riskfactor )
    {
        var baseUrl = "https://ai.airstack.xyz";
        var bearerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOiJjbGtheWNpdjkwMHk5bWIwODl0cm5xbGdwIiwiaXNzIjoicHJpdnkuaW8iLCJpYXQiOjE2ODk5NDQ1NjAsImF1ZCI6ImNsZzZseDV3MDAwMGZqdjA4eGV6aWgwbnEiLCJzdWIiOiJkaWQ6cHJpdnk6Y2xrYXljaXcxMDB5Ym1iMDhoejB5a3FncyIsImV4cCI6MTY4OTk0ODE2MH0._qppHE7e2LI0gomQroeu9xxbBzMEacnHKg6wrcyNfXg0heGGHa30wSl5g_CGDhb6Sdh1CMSqlVNWoScEnFyL7A";
        var origin = "https://app.airstack.xyz";

        var question = "get the best ERC20 token on polygon.";
        var bestToken = AirstackApiClient.AirstackApiClient.MakeAirstackRequest(baseUrl, bearerToken, origin, question, riskfactor);
        return bestToken;
    }
    
    public string SwapTokenWithOneInch(string src, string destination, int amount ,int slippage )
    {
        var oneinchtrade = new OneInchSwapPolygon(_executionPerscription.ProxyAccount);
        return "0x6f5917224687158f64a62df8c2c3470b928f3fa4da48deb57eb82158718e592a";
    }
}
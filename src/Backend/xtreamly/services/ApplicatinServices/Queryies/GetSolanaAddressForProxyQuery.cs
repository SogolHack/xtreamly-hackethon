using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetSolanaAddressForProxyQuery : IRequest<OneOf<string,Exception>>
{
    public GetSolanaAddressForProxyQuery(string proxyId ,string userId )
    {
        UserId = userId;
        ProxyId = proxyId;
    }

    public string  UserId { get; set; }
    public string ProxyId { get; set; }
}
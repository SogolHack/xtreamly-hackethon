using System.Runtime.Intrinsics.Arm;
using System.Security.Cryptography;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.Enums;
using Xtreamly.Services.ScriptExecutionService.Commands;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetDidQueryHandler : IRequestHandler<GetDidQuery, OneOf<string, Exception>>
{
    private readonly IMediator _mediator;
    private readonly ApplicationDbContext _applicationDbContext;

    public GetDidQueryHandler(ApplicationDbContext applicationDbContext, IMediator mediator)
    {
        _applicationDbContext = applicationDbContext;
        _mediator = mediator;
    }

    public async Task<OneOf<string, Exception>> Handle(GetDidQuery request, CancellationToken cancellationToken)
    {
        try
        {

            return  "false";
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
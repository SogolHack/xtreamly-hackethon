using MediatR;
using Microsoft.EntityFrameworkCore;
using NBitcoin;
using OneOf;
using Xtreamly.Models;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetBitcoinAddressForProxyQueryHandler : IRequestHandler<GetBitcoinAddressForProxyQuery, OneOf.OneOf<string,Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetBitcoinAddressForProxyQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<string, Exception>> Handle(GetBitcoinAddressForProxyQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var proxyAccount =
                await _applicationDbContext.ProxyAccounts.Include(proxy => proxy.Owner).SingleOrDefaultAsync(proxy => proxy.Id == request.ProxyId, cancellationToken: cancellationToken);
            if (proxyAccount == null)
            {
                return new Exception("no such a proxy");
            }

            if (!proxyAccount.Active)
            {
                return new Exception("proxy account is deactivated");
            }

            if (proxyAccount.Owner.Id != request.UserId)
            {
                return new Exception("you do not have access the specified proxy account");
            }
            var mnemonic = proxyAccount.Mnemonica;
            var extKey = new Mnemonic(mnemonic).DeriveExtKey();
            var keyPath = new KeyPath("m/44'/0'/0'/0/0");
            var key = extKey.Derive(keyPath).PrivateKey;
            var address = key.PubKey.GetAddress(ScriptPubKeyType.Segwit, Network.Main);
            return address.ToString();
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
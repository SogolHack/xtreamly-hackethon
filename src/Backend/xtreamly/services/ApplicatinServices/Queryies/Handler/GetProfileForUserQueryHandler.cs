using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetProfileForUserQueryHandler : IRequestHandler<GetProfileForUserQuery, OneOf<XtreamlyUser, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetProfileForUserQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<XtreamlyUser, Exception>> Handle(GetProfileForUserQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);

            if (user == null)
            {
                return new Exception("no such a user");
            }

            return user;
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
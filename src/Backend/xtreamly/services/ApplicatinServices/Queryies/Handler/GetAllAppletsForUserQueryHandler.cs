using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetAllAppletsForUserQueryHandler : IRequestHandler<GetAllAppletsForUserQuery, OneOf<List<Applet>, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetAllAppletsForUserQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<List<Applet>, Exception>> Handle(GetAllAppletsForUserQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var applets = _applicationDbContext.Applets
                .Include(applet => applet.Owner)
                .Where(applet => applet.Owner.Id == request.UserId)
                .Include(applet =>  applet.Script)
                .Skip(request.PaginationDto.Skip)
                .Take(request.PaginationDto.Count);
            if (!await applets.AnyAsync(cancellationToken: cancellationToken))
            {
                return new List<Applet>();
            }

            return await applets.ToListAsync(cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Solnet.Wallet;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetSolanaAddressForProxyQueryHandler : IRequestHandler<GetSolanaAddressForProxyQuery, OneOf<string, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetSolanaAddressForProxyQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<string, Exception>> Handle(GetSolanaAddressForProxyQuery request, CancellationToken cancellationToken)
    {
        try
        {

            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var proxyAccount =
                await _applicationDbContext.ProxyAccounts.Include(proxy => proxy.Owner).SingleOrDefaultAsync(proxy => proxy.Id == request.ProxyId, cancellationToken: cancellationToken);
            if (proxyAccount == null)
            {
                return new Exception("no such a proxy");
            }
            if (!proxyAccount.Active)
            {
                return new Exception("proxy account is deactivated");
            }

            if (proxyAccount.Owner.Id != request.UserId)
            {
                return new Exception("you do not have access the specified proxy account");
            }
            var mnemonic = proxyAccount.Mnemonica;
            var keyPair = new Wallet(mnemonic).Account;
            return keyPair.PublicKey.ToString();

        }
        catch (Exception e)
        {
            return e;
        }
    }
}
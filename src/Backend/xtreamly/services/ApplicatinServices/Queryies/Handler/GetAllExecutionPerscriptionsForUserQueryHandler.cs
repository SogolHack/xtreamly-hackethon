using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetAllExecutionPerscriptionsForUserQueryHandler : IRequestHandler<GetAllExecutionPerscriptionsForUserQuery, OneOf<List<ExecutionPerscription>, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetAllExecutionPerscriptionsForUserQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<List<ExecutionPerscription>, Exception>> Handle(GetAllExecutionPerscriptionsForUserQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var result = _applicationDbContext.ExecutionPerscriptions
                .Include(perscription => perscription.ProxyAccount)
                .ThenInclude(account => account.Owner)
                .Include(perscription => perscription.Applet)
                .Where(perscription => perscription.ProxyAccount.Owner.Id == request.UserId)
                .Skip(request.PaginationDto.Skip)
                .Take(request.PaginationDto.Count);

            if (!await result
                    .AnyAsync(cancellationToken: cancellationToken))
            {
                return new List<ExecutionPerscription>();
            }

            return await result.ToListAsync(cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
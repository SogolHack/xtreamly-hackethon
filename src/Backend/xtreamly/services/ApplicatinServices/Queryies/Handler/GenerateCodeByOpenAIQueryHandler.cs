using System.Text.Json.Serialization;
using ChatGPT.Net;
using MediatR;
using OneOf;


namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GeneratePermissionCodeByOpenAIQueryHandler : IRequestHandler<GeneratePermissionCodeByOpenAIQuery, OneOf<string, Exception>>
{
    public partial class OpenAiPayload
    {
        [JsonPropertyName("model")]
        public string Model { get; set; }

        [JsonPropertyName("messages")]
        public Message[] Messages { get; set; }

        [JsonPropertyName("temperature")]
        public decimal Temperature { get; set; }
    }

    public partial class Message
    {
        [JsonPropertyName("role")]
        public string Role { get; set; }

        [JsonPropertyName("content")]
        public string Content { get; set; }
    }
    public async Task<OneOf<string, Exception>> Handle(GeneratePermissionCodeByOpenAIQuery requestText, CancellationToken cancellationToken)
    {
   
        try
        {
            var prepend = """
                I want to write a lua script for a specific runtime.

                DO NOT OUTPUT ANY EXPLANATION BY YOURSELF, ONLY CODE

                the runtime has the following external function that you can use without implementing them yourself
                1-time(): returns the number of milli seconds from 1.1.1970
                2-getMemory() returns the remaining available memory for the script in bytes
                3-httpGet(url) which takes the url as the parameter and download the resource and return the result as string
                there are also 3 constants available in the code : 
                _DAPP : which is the the identifier of the party making the request.
                _PROXY : which is the identification of the proxy account this script is running on the behave of.
                _DID : which is the identification of the required resource by the _DAPP

                this is going to be a control script, meaning finally it should return true or false.
                true means the requesting _DAPP has access to the specified _DID through the _PROXY
                false means  the requesting _DAPP has no access to the specified _DID through the _PROXY
                
                for example if the script is just return true; it will allow anyone to access the data.
                
                Do NOT access to 'os' and 'io' functions
                given this context write a lua script that : 
        """;

            var result = prepend + requestText.Text;
            var ApiKey = "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";
             var bot = new ChatGpt(ApiKey);
             bot.ClearConversations();
             var response = await bot.Ask(result);
             return (response);

        }
        catch (Exception e)
        {
            return e;
        }
    }
}
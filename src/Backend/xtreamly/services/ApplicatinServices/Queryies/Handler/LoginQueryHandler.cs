using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NBitcoin;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class LoginQueryHandler : IRequestHandler<LoginQuery, OneOf<string, Exception>>
{
    private readonly IConfiguration _configuration;
    private readonly ApplicationDbContext _applicationDbContext;
    private readonly UserManager<XtreamlyUser> _XtreamlyUserUserManager;
    private readonly IMediator _mediator;
    private readonly RoleManager<IdentityRole> _roleManager;

    public LoginQueryHandler(ApplicationDbContext applicationDbContext, UserManager<XtreamlyUser> xtreamlyUserUserManager, IMediator mediator, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
    {
        _applicationDbContext = applicationDbContext;
        _XtreamlyUserUserManager = xtreamlyUserUserManager;
        _mediator = mediator;
        _roleManager = roleManager;
        _configuration = configuration;
    }

    public async Task<OneOf<string, Exception>> Handle(LoginQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user =await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.UserName == request.LoginDto.Email, cancellationToken: cancellationToken);
            
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var authenticationResult =
                await _XtreamlyUserUserManager.CheckPasswordAsync(user, request.LoginDto.Password);

            if (!authenticationResult)
            {
                return new Exception("invalid password");
            }

            
            var tokenHandler = new JwtSecurityTokenHandler();
            var authSigningKey = Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]);

            var tokenCapabilities = new List<TokenCapabilities>()
            {
                TokenCapabilities.EXECUTE_CODE,
                TokenCapabilities.FETHC_ALL_DID,
                TokenCapabilities.FETCH_LIST_OF_PROXY,
                TokenCapabilities.FETCH_INDIVISUAL_DID,
                TokenCapabilities.GENERATE_OTHER_TOKENS,
                TokenCapabilities.MANAGE_PROXY_ACCOUNT,
                TokenCapabilities.PUBLISH_APPLET,
                TokenCapabilities.GET_PROXY_ADDRESS,
                TokenCapabilities.FETCH_ALL_APPLETS
            };
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new("capabilities", System.Text.Json.JsonSerializer.Serialize(tokenCapabilities)),
                    new(ClaimTypes.Name, user.UserName!),
                    new(ClaimTypes.Role, UserRoles.General),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new(ClaimTypes.NameIdentifier, user.Id)
                }),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(authSigningKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwt = tokenHandler.WriteToken(token);
            return jwt;
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetAllAppletsQueryHandler : IRequestHandler<GetAllAppletsQuery, OneOf<List<Applet>, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetAllAppletsQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<List<Applet>, Exception>> Handle(GetAllAppletsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var applets = _applicationDbContext.Applets
                .Where(applet => applet.Visibility == AppletVisibility.PUBLIC)
                .Include(applet => applet.Owner)
                .Include(applet => applet.Script)
                .Skip(request.PaginationDto.Skip)
                .Take(request.PaginationDto.Count );
            if (!await applets.AnyAsync(cancellationToken: cancellationToken))
            {
                return new List<Applet>();
            }

            return await applets.ToListAsync(cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
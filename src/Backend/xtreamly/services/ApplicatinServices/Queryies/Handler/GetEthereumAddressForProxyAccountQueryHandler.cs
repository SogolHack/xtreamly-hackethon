using MediatR;
using Microsoft.EntityFrameworkCore;
using NBitcoin;
using Nethereum.HdWallet;
using OneOf;
using Xtreamly.Models;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetEthereumAddressForProxyAccountQueryHandler : IRequestHandler<GetEthereumAddressForProxyAccountQuery, OneOf<string, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetEthereumAddressForProxyAccountQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<string, Exception>> Handle(GetEthereumAddressForProxyAccountQuery request, CancellationToken cancellationToken)
    {
        try
        {

            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var proxyAccount =
                await _applicationDbContext.ProxyAccounts.Include(proxy => proxy.Owner).SingleOrDefaultAsync(proxy => proxy.Id == request.ProxyId, cancellationToken: cancellationToken);
            if (proxyAccount == null)
            {
                return new Exception("no such a proxy");
            }
            if (!proxyAccount.Active)
            {
                return new Exception("proxy account is deactivated");
            }

            if (proxyAccount.Owner.Id != request.UserId)
            {
                return new Exception("you do not have access the specified proxy account");
            }
            var publickey = (new Wallet(new Mnemonic(proxyAccount.Mnemonica).DeriveSeed())).GetAccount(0).Address;
            return publickey;

        }
        catch (Exception e)
        {
            return e;
        }
    }
}
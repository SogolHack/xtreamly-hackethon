using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetProxyAccountForUserQueryHandler : IRequestHandler<GetProxyAccountForUserQuery, OneOf<List<ProxyAccount>, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetProxyAccountForUserQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<List<ProxyAccount>, Exception>> Handle(GetProxyAccountForUserQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _applicationDbContext.XtreamlyUsers.AnyAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (!user)
            {
                return new Exception("no such a user");
            }

            var proxies  =  _applicationDbContext.ProxyAccounts.Include(proxy => proxy.Owner)
                .Where(proxy => proxy.Owner.Id == request.UserId).Skip(request.PaginationDto.Skip).Take(request.PaginationDto.Count);

            if (!await proxies.AnyAsync(cancellationToken: cancellationToken))
            {
                return new List<ProxyAccount>();
            }

            return proxies.ToList();
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
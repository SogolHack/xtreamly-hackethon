using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GenerateTokenForAccountQueryHandler : IRequestHandler<GenerateTokenForAccountQuery, OneOf<string ,Exception>>
{
    private readonly IConfiguration _configuration;
    private readonly ApplicationDbContext _applicationDbContext;

    public GenerateTokenForAccountQueryHandler(ApplicationDbContext applicationDbContext, IConfiguration configuration)
    {
        _applicationDbContext = applicationDbContext;
        _configuration = configuration;
    }

    public async Task<OneOf<string, Exception>> Handle(GenerateTokenForAccountQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }
              
            var tokenHandler = new JwtSecurityTokenHandler();
            var authSigningKey = Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]);


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new("capabilities", System.Text.Json.JsonSerializer.Serialize(request.CapabilitiesList)),
                    new(ClaimTypes.Name, user.UserName!),
                    new(ClaimTypes.Role, UserRoles.General),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new(ClaimTypes.NameIdentifier, user.Id),
                }),
                Expires = request.ExpireTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(authSigningKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwt = tokenHandler.WriteToken(token);
            return jwt;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}
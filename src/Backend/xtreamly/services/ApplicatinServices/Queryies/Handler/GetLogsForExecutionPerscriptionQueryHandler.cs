using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetLogsForExecutionPerscriptionQueryHandler : IRequestHandler<GetLogsForExecutionPerscriptionQuery, OneOf.OneOf<List<ExecutionReport>, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetLogsForExecutionPerscriptionQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<List<ExecutionReport>, Exception>> Handle(GetLogsForExecutionPerscriptionQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var result = _applicationDbContext.ExecutionReports
                .Where(report => report.ExecutionPerscriptionId == request.ExecutionPerscriptionId)
                .OrderBy(report => report.DateTime)
                .Skip(request.PaginationDto.Skip)
                .Take(request.PaginationDto.Count);

            if (!await result.AnyAsync(cancellationToken: cancellationToken))
            {
                return new List<ExecutionReport>();
            }


            return (await result.ToListAsync(cancellationToken: cancellationToken));
        }
        catch (Exception e)
        {
            return e;   
        }
    }
}
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Queryies.Handler;

public class GetPermissionAppletForUserQueryHandler : IRequestHandler<GetPermissionAppletForUserQuery, OneOf<Applet, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public GetPermissionAppletForUserQueryHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<Applet, Exception>> Handle(GetPermissionAppletForUserQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user =>
                user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var permissionApplet = await _applicationDbContext.Applets.Include(applet => applet.Owner).Include(applet => applet.Script)
                .SingleOrDefaultAsync(applet => applet.Owner.Id == request.UserId && applet.AppletType == AppletType.PERMISSION, cancellationToken: cancellationToken);
            
            if (permissionApplet == null)
            {
                return new Exception( "user has no permission applet, all permission requests will be denied ");
            }

            return permissionApplet;
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
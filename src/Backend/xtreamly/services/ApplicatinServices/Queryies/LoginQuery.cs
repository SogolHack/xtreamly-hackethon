using MediatR;
using OneOf;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class LoginQuery : IRequest<OneOf<string, Exception>>
{
    public LoginQuery(LoginDto loginDto)
    {
        LoginDto = loginDto;
    }

    public LoginDto LoginDto { get; set; }
    
}
using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetEthereumAddressForProxyAccountQuery : IRequest<OneOf<string,Exception>>
{
    public GetEthereumAddressForProxyAccountQuery(string proxyId , string userId)
    {
        UserId = userId;
        ProxyId = proxyId;
    }
    
    public string UserId { get; set; }
    public string ProxyId { get; set; }
}
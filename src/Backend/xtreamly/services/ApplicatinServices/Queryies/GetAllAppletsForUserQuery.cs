using MediatR;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetAllAppletsForUserQuery : IRequest<OneOf<List<Applet>, Exception>>
{
    public GetAllAppletsForUserQuery(PaginationDto paginationDto, string userId)
    {
        PaginationDto = paginationDto;
        UserId = userId;
    }

    public PaginationDto PaginationDto { get; set; }
    public string UserId { get; set; }
}
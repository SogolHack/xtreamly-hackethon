using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetBitcoinAddressForProxyQuery : IRequest<OneOf<string ,Exception>>
{
    public GetBitcoinAddressForProxyQuery(string proxyId, string userId)
    {
        UserId = userId;
        ProxyId = proxyId;
    }

    public string UserId { get; set; }
    public string ProxyId { get; set; }
}
using MediatR;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetAllExecutionPerscriptionsForUserQuery : IRequest<OneOf<List<ExecutionPerscription>, Exception>>
{
    public GetAllExecutionPerscriptionsForUserQuery(PaginationDto paginationDto, string userId)
    {
        PaginationDto = paginationDto;
        UserId = userId;
    }

    public PaginationDto PaginationDto { get; set; }
    
    public string UserId { get; set; }
}
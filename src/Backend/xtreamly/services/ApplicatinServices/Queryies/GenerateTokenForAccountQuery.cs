using MediatR;
using OneOf;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GenerateTokenForAccountQuery : IRequest<OneOf<string,Exception>>
{
    public GenerateTokenForAccountQuery(string userId, List<TokenCapabilities> capabilitiesList, DateTime expireTime)
    {
        CapabilitiesList = capabilitiesList;
        ExpireTime = expireTime;
        UserId = userId;
    }
    
    
    public DateTime ExpireTime { get; set; }
    public List<TokenCapabilities> CapabilitiesList { get; set; }
    public string UserId { get; set; }
}
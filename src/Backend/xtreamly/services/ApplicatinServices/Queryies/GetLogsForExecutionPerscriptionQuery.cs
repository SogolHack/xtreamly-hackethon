using MediatR;
using OneOf;
using Xtreamly.Models;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetLogsForExecutionPerscriptionQuery : IRequest<OneOf<List<ExecutionReport> , Exception>>
{
    public GetLogsForExecutionPerscriptionQuery(string userId, string executionPerscriptionId, PaginationDto paginationDto)
    {
        UserId = userId;
        ExecutionPerscriptionId = executionPerscriptionId;
        PaginationDto = paginationDto;
    }

    public string UserId { get; set; }
    
    public string ExecutionPerscriptionId { get; set; }
    
    public PaginationDto PaginationDto { get; set; }
}
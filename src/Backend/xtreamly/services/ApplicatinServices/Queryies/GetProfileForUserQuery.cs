using MediatR;
using OneOf;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetProfileForUserQuery : IRequest<OneOf<XtreamlyUser, Exception>>
{
    public GetProfileForUserQuery(string userId)
    {
        UserId = userId;
    }

    public string UserId { get; set; }
}
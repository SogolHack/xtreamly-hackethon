using MediatR;
using OneOf;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetPermissionAppletForUserQuery: IRequest<OneOf<Applet, Exception>>
{
    public GetPermissionAppletForUserQuery(string userId)
    {
        UserId = userId;
    }

    public string UserId { get; set; }
}
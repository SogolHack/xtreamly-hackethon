using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetDidQuery: IRequest<OneOf<string, Exception>>
{
    public GetDidQuery(string userId, string did, string dappSecret, string proxyId)
    {
        UserId = userId;
        Did = did;
        DappSecret = dappSecret;
        ProxyId = proxyId;
    }

    public string ProxyId { get; set; }
    public string UserId { get; set; }
    public string Did { get; set; }
    public string DappSecret { get; set; }
}
using MediatR;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetAllExecutionPerscriptionsForUserByProxyIdQuery : IRequest<OneOf.OneOf<List<ExecutionPerscription>, Exception>>
{
    public GetAllExecutionPerscriptionsForUserByProxyIdQuery(PaginationDto paginationDto, string userId, string proxyId)
    {
        PaginationDto = paginationDto;
        UserId = userId;
        ProxyId = proxyId;
    }

    public PaginationDto PaginationDto { get; set; }
    
    public string UserId { get; set; }
    
    public string ProxyId { get; set; }
}
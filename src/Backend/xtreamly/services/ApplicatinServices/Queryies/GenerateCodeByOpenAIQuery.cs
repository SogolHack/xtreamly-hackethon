using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GeneratePermissionCodeByOpenAIQuery : IRequest<OneOf<string, Exception>>
{
    public GeneratePermissionCodeByOpenAIQuery(string text)
    {
        Text = text;
    }

    public string Text { get; set; }
}
using MediatR;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Queryies;

public class GetAllAppletsQuery : IRequest<OneOf<List<Applet>, Exception>>
{
    public GetAllAppletsQuery(PaginationDto paginationDto)
    {
        PaginationDto = paginationDto;
    }

    public PaginationDto PaginationDto { get; set; }
}
using MediatR;
using OneOf;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class CreateAppletRunnerCommand : IRequest<OneOf<string, Exception>>
{
    public CreateAppletRunnerCommand(string appletId, string proxyId, string cron, bool active)
    {
        AppletId = appletId;
        ProxyId = proxyId;
        Cron = cron;
        Active = active;
    }

    public string AppletId { get; set; }
    
    public string ProxyId { get; set; }
    
    public string Cron { get; set; }
    
    public bool Active { get; set; }
}
using MediatR;
using OneOf;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class ChangeProxyAccountExecutionStatusCommand : IRequest<OneOf<bool, Exception>>
{
    public ChangeProxyAccountExecutionStatusCommand( string userId ,string proxyAccountId, ExecutionStatus executionStatus)
    {
        ProxyAccountId = proxyAccountId;
        ExecutionStatus = executionStatus;
        UserID = userId;
    }

    public string ProxyAccountId { get; set; }
    
    public ExecutionStatus ExecutionStatus { get; set; }
    
    public string UserID { get; set; }
}
using MediatR;
using Microsoft.EntityFrameworkCore;
using NBitcoin;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class ChangeProxyAccountActivisionCommandHandler : IRequestHandler<ChangeProxyAccountActivisionCommand, OneOf<bool, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public ChangeProxyAccountActivisionCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<bool, Exception>> Handle(ChangeProxyAccountActivisionCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user => user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var proxy = await _applicationDbContext.ProxyAccounts.SingleOrDefaultAsync(proxy =>
                proxy.Id == request.ProxyId, cancellationToken: cancellationToken);
            
            if (proxy == null)
            {
                return new Exception("no such a proxy");
            }
            proxy.Active = request.NewStatus;
            
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return true;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
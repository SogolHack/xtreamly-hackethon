using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class AddOrUpdateDynamicPermisionCommandHandler : IRequestHandler<AddOrUpdateDynamicPermisionCommand, OneOf.OneOf<bool, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public AddOrUpdateDynamicPermisionCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<bool, Exception>> Handle(AddOrUpdateDynamicPermisionCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user => user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var permissionApplet =
                await _applicationDbContext.Applets.Include(applet => applet.Script).SingleOrDefaultAsync(applet =>
                    applet.AppletType == AppletType.PERMISSION, cancellationToken: cancellationToken);
            if (permissionApplet == null)
            {
                var newPermissionApplet = new Applet()
                {
                    AppletType = AppletType.PERMISSION,
                    Metadata = request.AppletDto.Metadata,
                    Script = new Script()
                    {
                        ScriptText = request.AppletDto.Script,
                        ScriptCompabilityVersion = 1
                    },
                    Owner = user,
                    Visibility = request.AppletDto.AppletVisibility,
                    CreationDate = DateTime.UtcNow,
                    AppletCompabilityVersion = 1
                };
                 await _applicationDbContext.Applets.AddAsync(newPermissionApplet, cancellationToken);
            }
            else
            {
                var script = await _applicationDbContext.Scripts.SingleOrDefaultAsync(
                    script => script.Id == permissionApplet.Script.Id, cancellationToken: cancellationToken);

                if (script == null)
                {
                    return new Exception(
                        "no such a script in register for this applet, this should not normally happen");
                }

                script.ScriptText = request.AppletDto.Script;
            }
           
            
            
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return true;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
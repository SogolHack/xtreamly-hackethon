using MediatR;
using Xtreamly.Models;
using Xtreamly.Models.Enums;
using Xtreamly.Services.FunctionalityProviders;
using Xtreamly.Services.ScriptExecutionService.Commands;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class ExecuteAppletCommandHandler : IRequestHandler<ExecuteAppletCommand>
{
    private readonly ApplicationDbContext _applicationDbContext;

    private readonly IMediator _mediator;

    private readonly ILogger<ExecuteAppletCommandHandler> _logger;

    private readonly GeneralAppletFunctionalityProvider _generalAppletFunctionalityProvider;

    public ExecuteAppletCommandHandler(ApplicationDbContext applicationDbContext, IMediator mediator, ILogger<ExecuteAppletCommandHandler> logger ,GeneralAppletFunctionalityProvider generalAppletFunctionalityProvider )
    {
        _applicationDbContext = applicationDbContext;
        _mediator = mediator;
        _logger = logger;
        _generalAppletFunctionalityProvider = generalAppletFunctionalityProvider;
    }

    public async Task Handle(ExecuteAppletCommand request, CancellationToken cancellationToken)
    {
        if (request.ExecutionPerscription.ProxyAccount.ExecutionStatus != ExecutionStatus.RUNNING ||
            !request.ExecutionPerscription.ProxyAccount.Active || ! request.ExecutionPerscription.Active)
        {
            _logger.LogInformation("execution perscription with id {PerscriptionId} did not run because the proxy account {ProxyAccountId} is not allowed to execute at this status, maybe proxy account is disabled or pause or  maybe execution prescription is not active", request.ExecutionPerscription, request.ExecutionPerscription.ProxyAccount.Id);
            return;
        }
        try
        {
            if (request.ExecutionPerscription.Applet.AppletType == AppletType.GENERAL)
            {
                var functinalityProvider = _generalAppletFunctionalityProvider;
                var executionPerscription = request.ExecutionPerscription;
                functinalityProvider.SetApplet(ref executionPerscription);
                
                await _mediator.Send(new ExecuteScriptCommand(request.ExecutionPerscription.Applet.Script.Id,
                  functinalityProvider), cancellationToken);
                _logger.LogInformation("execution perscription with id {PerscriptionId} was executed", request.ExecutionPerscription);

            }
        }
        catch (Exception e)
        {
            _logger.LogError("failed to execute execution perscription with id  {PerscriptionId} : {ErrorMessage}" , request.ExecutionPerscription.Id, e.Message);
        }
    }
}
using System.Transactions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class SignupCommandHandler : IRequestHandler<SignupCommand, OneOf.OneOf<bool, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;
    private readonly UserManager<XtreamlyUser> _XtreamlyUserUserManager;
    private readonly IMediator _mediator;
    private readonly RoleManager<IdentityRole> _roleManager;

    public SignupCommandHandler(ApplicationDbContext applicationDbContext, IMediator mediator, RoleManager<IdentityRole> roleManager, UserManager<XtreamlyUser> xtreamlyUserUserManager)
    {
        _applicationDbContext = applicationDbContext;
        _mediator = mediator;
        _roleManager = roleManager;
        _XtreamlyUserUserManager = xtreamlyUserUserManager;
    }

    public async Task<OneOf<bool, Exception>> Handle(SignupCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var companyUserExists =
                await _XtreamlyUserUserManager.FindByNameAsync(request.SignupDto.Email);
            if (companyUserExists != null)
            {
                await transaction.RollbackAsync(cancellationToken);
                return new Exception("email already exist");
            }

            var user = new XtreamlyUser()
            {
                UserName = request.SignupDto.Email,
                Email = request.SignupDto.Email,
            };
            var result =
                await _XtreamlyUserUserManager.CreateAsync(user, request.SignupDto.Password);
            if (!result.Succeeded)
            {
                await transaction.RollbackAsync(cancellationToken);
                return new Exception("User creation failed! Please check user details and try again." +
                                     Environment.NewLine + string.Join(Environment.NewLine,
                                         result.Errors.Select(err => err.Description)));
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.General))
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.General));
            await _XtreamlyUserUserManager.AddToRoleAsync(user, UserRoles.General);
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
                    
            await transaction.CommitAsync(cancellationToken);

            return true;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
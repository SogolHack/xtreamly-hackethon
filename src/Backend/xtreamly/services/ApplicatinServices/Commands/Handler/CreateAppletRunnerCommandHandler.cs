using Hangfire;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class CreateAppletRunnerCommandHandler : IRequestHandler<CreateAppletRunnerCommand, OneOf<string, Exception>>
{
    private readonly IBackgroundJobClient _backgroundJobs;
    private readonly ApplicationDbContext _applicationDbContext;

    public CreateAppletRunnerCommandHandler(ApplicationDbContext applicationDbContext, IBackgroundJobClient backgroundJobs)
    {
        _applicationDbContext = applicationDbContext;
        _backgroundJobs = backgroundJobs;
    }

    public async Task<OneOf<string, Exception>> Handle(CreateAppletRunnerCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            
            var proxy = await _applicationDbContext.ProxyAccounts.Include(proxy => proxy.Owner).SingleOrDefaultAsync(proxy =>
                proxy.Id == request.ProxyId, cancellationToken: cancellationToken);
            
            if (proxy == null)
            {
                return new Exception("no such a proxy");
            }

            if (!proxy.Active)
            {
                return new Exception("proxy account is deactivated");
            }

            var applet = await  _applicationDbContext.Applets.Include(applet => applet.Owner).SingleOrDefaultAsync(applet =>
                applet.AppletType == AppletType.GENERAL && applet.Id == request.AppletId, cancellationToken: cancellationToken);
            if (applet == null)
            {
                return new Exception("no such an applet");
            }

            if (applet.Owner.Id != proxy.Owner.Id && applet.Visibility != AppletVisibility.PUBLIC)
            {
                return new Exception("applet is not available for you");
            }

            var ex = new ExecutionPerscription()
            {
                Active = true,
                Applet = applet,
                Cron = request.Cron,
                ProxyAccount = proxy
            };

          var result =   await _applicationDbContext.ExecutionPerscriptions.AddAsync(ex, cancellationToken);
          
          
          await _applicationDbContext.SaveChangesAsync(cancellationToken);
          await transaction.CommitAsync(cancellationToken);

          return result.Entity.Id;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
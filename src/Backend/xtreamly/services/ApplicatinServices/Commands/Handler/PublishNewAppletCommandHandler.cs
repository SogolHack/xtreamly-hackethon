using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class PublishNewAppletCommandHandler : IRequestHandler<PublishNewAppletCommand, OneOf<string ,Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public PublishNewAppletCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<string, Exception>> Handle(PublishNewAppletCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user => user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var applet = new Applet()
            {
                Metadata = request.AppletDto.Metadata,
                Owner = user,
                Script = new Script()
                {
                    ScriptCompabilityVersion = 1,
                    ScriptText = request.AppletDto.Script
                },
                AppletType = AppletType.GENERAL,
                CreationDate = DateTime.UtcNow,
                AppletCompabilityVersion = 1,
                Visibility = request.AppletDto.AppletVisibility
            };

            var result = await _applicationDbContext.Applets.AddAsync(applet, cancellationToken);
            
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);
            return result.Entity.Id;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
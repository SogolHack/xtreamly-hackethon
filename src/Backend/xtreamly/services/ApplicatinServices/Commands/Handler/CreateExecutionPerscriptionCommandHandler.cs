using Hangfire;
using Hangfire.Dashboard.Resources;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Quartz;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class
    CreateExecutionPerscriptionCommandHandler : IRequestHandler<CreateExecutionPerscriptionCommand,
        OneOf<string, Exception>>
{
    private readonly IMediator _mediator;
    private readonly ApplicationDbContext _applicationDbContext;

    public CreateExecutionPerscriptionCommandHandler(ApplicationDbContext applicationDbContext, IMediator mediator)
    {
        _applicationDbContext = applicationDbContext;
        _mediator = mediator;
    }

    public async Task<OneOf<string, Exception>> Handle(CreateExecutionPerscriptionCommand request,
        CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database
            .BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers
                    .SingleOrDefaultAsync(user => user.Id == request.UserId, cancellationToken);
            if (user == null) return new Exception("no such a user");

            var proxy = await _applicationDbContext.ProxyAccounts
                .SingleOrDefaultAsync(account =>
                    account.Id == request.ExecutionPerscriptionDto.ProxyAccountId, cancellationToken);
            if (proxy == null) return new Exception("no such a proxy");

            var applet = await _applicationDbContext.Applets
                .Include(applet => applet.Owner)
                .Include(applet => applet.Script)
                .SingleOrDefaultAsync(applet => applet.Id == request.ExecutionPerscriptionDto.AppletId &&
                                                (applet.Visibility == AppletVisibility.PUBLIC ||
                                                 applet.Owner.Id == request.UserId), cancellationToken);

            if (applet == null) return new Exception("applet does not exists or is not available to the user");

            var cronValidation = CronExpression
                .IsValidExpression(request.ExecutionPerscriptionDto.Cron);
              
            
            if (!cronValidation) return new Exception("cron string is not valid");

            var executionPerscription = new ExecutionPerscription
            {
                Active = request.ExecutionPerscriptionDto.Active,
                Applet = applet,
                Metadata = request.ExecutionPerscriptionDto.Metadata,
                ProxyAccount = proxy,
                Cron = request.ExecutionPerscriptionDto.Cron
            };


            
            var result =
                await _applicationDbContext.ExecutionPerscriptions
                    .AddAsync(executionPerscription, cancellationToken);

            await _applicationDbContext
                .SaveChangesAsync(cancellationToken);
            await transaction
                .CommitAsync(cancellationToken);
            
            if (executionPerscription.Active)
            {
                RecurringJob
                    .AddOrUpdate(result.Entity.Id,() =>  _mediator.Send(new ExecuteAppletCommand(result.Entity) , new CancellationToken()) , request.ExecutionPerscriptionDto.Cron);
            }
            return result.Entity.Id;
        }

        catch (Exception e)
        {
            await transaction
                .RollbackAsync(cancellationToken);
            return e;
        }
    }
}
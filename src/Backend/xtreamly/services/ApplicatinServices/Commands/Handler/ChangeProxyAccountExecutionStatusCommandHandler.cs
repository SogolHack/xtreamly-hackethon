using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class ChangeProxyAccountExecutionStatusCommandHandler : IRequestHandler<ChangeProxyAccountExecutionStatusCommand, OneOf<bool, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public ChangeProxyAccountExecutionStatusCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<bool, Exception>> Handle(ChangeProxyAccountExecutionStatusCommand request, CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var proxy = await _applicationDbContext.ProxyAccounts
                .Include(proxy => proxy.Owner)
                .SingleOrDefaultAsync(proxy =>
                proxy.Id == request.ProxyAccountId, cancellationToken: cancellationToken);
            if (proxy == null)
            {
                return new Exception("no such a proxy");
            }

            if (proxy.Owner.Id != request.UserID)
            {
                return new Exception("access denied, user is not the owner of the proxy account");
            }
            
            proxy.ExecutionStatus = request.ExecutionStatus;
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return true;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
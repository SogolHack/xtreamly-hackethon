using MediatR;
using Microsoft.EntityFrameworkCore;
using NBitcoin;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class CreateNewProxyAccountCommandHandler : IRequestHandler<CreateNewProxyAccountCommand, OneOf<string, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public CreateNewProxyAccountCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<string, Exception>> Handle(CreateNewProxyAccountCommand request, CancellationToken cancellationToken)
    {
        
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user => user.Id == request.OwnerId, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var mnemo = new Mnemonic(Wordlist.English, WordCount.Twelve);
            var proxy = await _applicationDbContext.ProxyAccounts.AddAsync(new ProxyAccount()
            {
                Name = request.ProxyAccountDto.Name,
                Owner = user,
                CreationDate = DateTime.UtcNow,
                Mnemonica = mnemo.ToString(),
                Active = true,
                ExecutionStatus = ExecutionStatus.RUNNING
            }, cancellationToken);
            
            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return proxy.Entity.Id;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }

    }
}
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Models.Enums;

namespace Xtreamly.Services.ApplicatinServices.Commands.Handler;

public class
    ChangeExecutionPerscripptionStatusCommandHandler : IRequestHandler<ChangeExecutionPerscripptionStatusCommand,
        OneOf<bool, Exception>>
{
    private readonly ApplicationDbContext _applicationDbContext;

    public ChangeExecutionPerscripptionStatusCommandHandler(ApplicationDbContext applicationDbContext)
    {
        _applicationDbContext = applicationDbContext;
    }

    public async Task<OneOf<bool, Exception>> Handle(ChangeExecutionPerscripptionStatusCommand request,
        CancellationToken cancellationToken)
    {
        await using var transaction = await _applicationDbContext.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var user =
                await _applicationDbContext.XtreamlyUsers.SingleOrDefaultAsync(user => user.Id == request.UserId,
                    cancellationToken: cancellationToken);
            if (user == null)
            {
                return new Exception("no such a user");
            }

            var executionPerscription =
                await _applicationDbContext.ExecutionPerscriptions
                    .Include(perscription => perscription.ProxyAccount)
                    .ThenInclude(account => account.Owner)
                    .SingleOrDefaultAsync(perscription => perscription.Id == request.UserId,
                        cancellationToken: cancellationToken);

            if (executionPerscription == null)
            {
                return new Exception("no such a execution perscription");
            }

            if (executionPerscription.ProxyAccount.Owner.Id != request.UserId)
            {
                return new Exception(
                    "access denied, you do not own the proxy account that holds this execution perscription");
            }

            executionPerscription.Active = request.ChangeExecutionPerscripption.NewStatus;


            await _applicationDbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return true;
        }

        catch (Exception e)
        {
            await transaction.RollbackAsync(cancellationToken);
            return e;
        }
    }
}
using MediatR;
using OneOf;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class CreateExecutionPerscriptionCommand : IRequest<OneOf<string, Exception>>
{
    public CreateExecutionPerscriptionCommand(ExecutionPerscriptionDto executionPerscriptionDto, string userId)
    {
        ExecutionPerscriptionDto = executionPerscriptionDto;
        UserId = userId;
    }

    public ExecutionPerscriptionDto ExecutionPerscriptionDto { get; set; }
    public string UserId { get; set; }
}
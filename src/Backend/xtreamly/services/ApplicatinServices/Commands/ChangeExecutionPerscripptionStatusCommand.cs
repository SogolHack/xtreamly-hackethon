using MediatR;
using OneOf;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class ChangeExecutionPerscripptionStatusCommand : IRequest<OneOf<bool, Exception>>
{
    public ChangeExecutionPerscripptionStatusCommand(ChangeExecutionPerscripptionDto changeExecutionPerscripption, string userId)
    {
        ChangeExecutionPerscripption = changeExecutionPerscripption;
        UserId = userId;
    }

    public ChangeExecutionPerscripptionDto ChangeExecutionPerscripption { get; set; }
    
    public string UserId { get; set; }
}
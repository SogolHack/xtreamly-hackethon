using MediatR;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class SignupCommand : IRequest<OneOf.OneOf<bool, Exception>>
{
    public SignupCommand(SignupDto signupDto)
    {
        SignupDto = signupDto;
    }

    public SignupDto SignupDto { get; set; }
}
using MediatR;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class ChangeProxyAccountActivisionCommand : IRequest<OneOf.OneOf<bool,Exception>>
{
 public ChangeProxyAccountActivisionCommand(string userId, string proxyId, bool newStatus)
 {
  UserId = userId;
  ProxyId = proxyId;
  NewStatus = newStatus;
 }

 public string UserId { get; set; }
 
 public string ProxyId { get; set; }
 
 public bool NewStatus { get; set; }
}
using MediatR;
using OneOf;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class AddOrUpdateDynamicPermisionCommand : IRequest<OneOf<bool, Exception>>
{
    public AddOrUpdateDynamicPermisionCommand(string userId, AppletDto appletDto)
    {
        UserId = userId;
        AppletDto = appletDto;
        
    }

    public string UserId { get; set; }
    
    public AppletDto AppletDto { get; set; }
}
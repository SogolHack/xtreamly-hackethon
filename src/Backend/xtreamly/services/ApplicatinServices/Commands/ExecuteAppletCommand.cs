using MediatR;
using Xtreamly.Models.DatabaseModels;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class ExecuteAppletCommand : IRequest
{
    public ExecuteAppletCommand(ExecutionPerscription executionPerscription)
    {
        ExecutionPerscription = executionPerscription;
    }

    public ExecutionPerscription ExecutionPerscription { get; set; }
}
using MediatR;
using OneOf;
using Xtreamly.Models.Dto;

namespace Xtreamly.Services.ApplicatinServices.Commands;

public class CreateNewProxyAccountCommand : IRequest<OneOf<string, Exception>>
{
    public CreateNewProxyAccountCommand(string ownerId, ProxyAccountDto proxyAccountDto)
    {
        OwnerId = ownerId;
        ProxyAccountDto = proxyAccountDto;
    }
    public ProxyAccountDto ProxyAccountDto { get; set; }
    public string OwnerId { get; set; }
}
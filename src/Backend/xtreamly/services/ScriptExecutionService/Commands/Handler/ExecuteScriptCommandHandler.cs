using System.Diagnostics;
using System.Net;
using System.Reflection;
using KeraLua;
using MediatR;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.EntityFrameworkCore;
using OneOf;
using Xtreamly.Models;
using Lua = NLua.Lua;

namespace Xtreamly.Services.ScriptExecutionService.Commands.Handler;

public class ExecuteScriptCommandHandler  : IRequestHandler<ExecuteScriptCommand, OneOf<object,Exception>>
{
    private readonly ILogger<ExecuteScriptCommandHandler> _logger;
    private Lua? _lua;
    private readonly ApplicationDbContext _applicationDbContext;

    public ExecuteScriptCommandHandler(ApplicationDbContext applicationDbContext, ILogger<ExecuteScriptCommandHandler> logger)
    {
        _applicationDbContext = applicationDbContext;
        _logger = logger;
    }
    
    
    public async Task<OneOf<object, Exception>> Handle(ExecuteScriptCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var script =
                await _applicationDbContext.Scripts.AsNoTracking().SingleOrDefaultAsync(script => script.Id == request.ScriptId, cancellationToken: cancellationToken);
            
            if (script == null)
            {
                _logger.LogError("execution error. no such a script : {Script}", request.ScriptId);

                return new Exception("no such a script");
            }
            
            var fuse = new FuseBuilder().WithMathLibrary().WithStringLibrary().WithTableLibrary().WithBaseLibrary().Build();
            _lua = new Lua(50000, 2000, true , fuse);
            var newInstance = request.FunctionalityProvider;
            newInstance.SetLua(ref _lua);
            var methods = request.FunctionalityProvider.GetType()
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => m.DeclaringType != typeof(object));
            
            foreach (var methodName in methods)
            {
                _lua.RegisterFunction(methodName.Name, newInstance!,  request.FunctionalityProvider.GetType().GetMethod(methodName.Name)!);
            }
            
            
            
            
          var result = await  Task.Run(() => _lua.DoString(script.ScriptText), cancellationToken);
            
          if (result == null || result.Length < 1)
            {
                _lua?.Dispose();
                return null;
            }
          _lua?.Dispose();
            return  result[0] ;
        }
        catch (Exception e)
        {
            _logger.LogError("execution error. {Error}", e.Message);
            _lua?.Dispose();
            return e;
        }
    }
}
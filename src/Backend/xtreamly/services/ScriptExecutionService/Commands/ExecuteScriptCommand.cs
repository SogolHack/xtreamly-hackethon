using MediatR;
using OneOf;
using Xtreamly.Models;
using Xtreamly.Services.FunctionalityProviders;

namespace Xtreamly.Services.ScriptExecutionService.Commands;

public class ExecuteScriptCommand : IRequest<OneOf<object, Exception>>
{
    public ExecuteScriptCommand(string scriptId, GeneralAppletFunctionalityProvider functionalityProvider)
    {
        ScriptId = scriptId;
        FunctionalityProvider = functionalityProvider;
    }

    public string ScriptId { get; set; }
    public GeneralAppletFunctionalityProvider FunctionalityProvider { get; set; }
}
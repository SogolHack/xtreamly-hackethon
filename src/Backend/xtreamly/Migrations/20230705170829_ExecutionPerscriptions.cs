﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Xtreamly.Migrations
{
    /// <inheritdoc />
    public partial class ExecutionPerscriptions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExecutionPerscriptions",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    AppletId = table.Column<string>(type: "TEXT", nullable: false),
                    ProxyAccountId = table.Column<string>(type: "TEXT", nullable: true),
                    Cron = table.Column<string>(type: "TEXT", nullable: false),
                    Active = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutionPerscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExecutionPerscriptions_Applets_AppletId",
                        column: x => x.AppletId,
                        principalTable: "Applets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutionPerscriptions_ProxyAccounts_ProxyAccountId",
                        column: x => x.ProxyAccountId,
                        principalTable: "ProxyAccounts",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionPerscriptions_AppletId",
                table: "ExecutionPerscriptions",
                column: "AppletId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionPerscriptions_ProxyAccountId",
                table: "ExecutionPerscriptions",
                column: "ProxyAccountId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutionPerscriptions");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Xtreamly.Migrations
{
    /// <inheritdoc />
    public partial class still_building : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProxyAccounts",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    SeedHexEncoded = table.Column<string>(type: "TEXT", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    OwnerId = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProxyAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProxyAccounts_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Scripts",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    ScriptCompabilityVersion = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scripts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Applets",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    Metadata = table.Column<string>(type: "TEXT", nullable: false),
                    ScriptId = table.Column<string>(type: "TEXT", nullable: true),
                    OwnerId = table.Column<string>(type: "TEXT", nullable: false),
                    AppletType = table.Column<int>(type: "INTEGER", nullable: false),
                    AppletCompabilityVersion = table.Column<double>(type: "REAL", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Applets_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applets_Scripts_ScriptId",
                        column: x => x.ScriptId,
                        principalTable: "Scripts",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Applets_OwnerId",
                table: "Applets",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Applets_ScriptId",
                table: "Applets",
                column: "ScriptId");

            migrationBuilder.CreateIndex(
                name: "IX_ProxyAccounts_OwnerId",
                table: "ProxyAccounts",
                column: "OwnerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Applets");

            migrationBuilder.DropTable(
                name: "ProxyAccounts");

            migrationBuilder.DropTable(
                name: "Scripts");
        }
    }
}

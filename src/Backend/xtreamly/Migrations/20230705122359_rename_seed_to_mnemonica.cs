﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Xtreamly.Migrations
{
    /// <inheritdoc />
    public partial class rename_seed_to_mnemonica : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SeedHexEncoded",
                table: "ProxyAccounts",
                newName: "Mnemonica");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Mnemonica",
                table: "ProxyAccounts",
                newName: "SeedHexEncoded");
        }
    }
}

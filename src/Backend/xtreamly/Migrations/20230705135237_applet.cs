﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Xtreamly.Migrations
{
    /// <inheritdoc />
    public partial class applet : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ScriptText",
                table: "Scripts",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Visibility",
                table: "Applets",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScriptText",
                table: "Scripts");

            migrationBuilder.DropColumn(
                name: "Visibility",
                table: "Applets");
        }
    }
}

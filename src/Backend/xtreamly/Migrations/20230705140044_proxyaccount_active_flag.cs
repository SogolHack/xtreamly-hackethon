﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Xtreamly.Migrations
{
    /// <inheritdoc />
    public partial class proxyaccount_active_flag : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ProxyAccounts",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "ProxyAccounts");
        }
    }
}

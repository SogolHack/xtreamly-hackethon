using System.Configuration;
using System.Text;
using Hangfire;
using Hangfire.Storage.SQLite;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.AspNetCore;
using Quartz.Impl;
using Quartz.Spi;
using Serilog;
using Xtreamly.Models;
using Xtreamly.Models.AuthorizationPolicy;
using Xtreamly.Models.DatabaseModels;
using Xtreamly.Services.FunctionalityProviders;
using Xtreamly.Utilities;


var builder = WebApplication.CreateBuilder(args);

var logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);



builder.Services.AddQuartz(q =>
{
    // base Quartz scheduler, job and trigger configuration
});

// ASP.NET Core hosting
builder.Services.AddQuartzServer(options =>
{
    // when shutting down we want jobs to complete gracefully
    options.WaitForJobsToComplete = true;
    options.AwaitApplicationStarted = true;
});
builder.Services.AddTransient(typeof(GeneralAppletFunctionalityProvider));


builder.Services.AddTransient<IAuthorizationHandler, GetProxyAccountAddressRequirmentHandler>();
builder.Services.AddTransient<IAuthorizationHandler, PublishAppletRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, ExecuteCodeRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, FetchAllDidRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, FetchListOfProxyRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, FetchIndividualDidRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, GenerateOtherTokensRequirementHandler>();
builder.Services.AddTransient<IAuthorizationHandler, CreateProxyAccountRequirementHandler>();

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("PublishAppletPolicy", policy =>
        policy.Requirements.Add(new PublishAppletRequirement()));
    
    options.AddPolicy("GetProxyAccountAddressPolicy", policy =>
        policy.Requirements.Add(new GetProxyAccountAddressRequirment()));

    options.AddPolicy("ExecuteCodePolicy", policy =>
        policy.Requirements.Add(new ExecuteCodeRequirement()));

    options.AddPolicy("FetchAllDidPolicy", policy =>
        policy.Requirements.Add(new FetchAllDidRequirement()));

    options.AddPolicy("FetchListOfProxyPolicy", policy =>
        policy.Requirements.Add(new FetchListOfProxyRequirement()));

    options.AddPolicy("FetchIndividualDidPolicy", policy =>
        policy.Requirements.Add(new FetchIndividualDidRequirement()));

    options.AddPolicy("GenerateOtherTokensPolicy", policy =>
        policy.Requirements.Add(new GenerateOtherTokensRequirement()));
    
    options.AddPolicy("ManageProxyAccountPolicy", policy =>
        policy.Requirements.Add(new CreateProxyAccountRequirement()));
});

builder.Services.AddControllers(options =>
{
    options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
});

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly));


builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    if (builder.Environment.IsDevelopment())
        options.UseSqlite("Data Source=Xtreamly.db;");
    else if (builder.Environment.IsProduction())
    {
        var databaseConnectionString = Environment.GetEnvironmentVariable("dbPath") ??
                                       throw new Exception("No database path is provided");
        var databaseUser = Environment.GetEnvironmentVariable("dbUser") ??
                           throw new Exception("No database user is provided");
        var databasePassword = Environment.GetEnvironmentVariable("dbPassword") ??
                               throw new Exception("No database password is provided");
        var databasePort = Environment.GetEnvironmentVariable("dbport") ??
                           throw new Exception("No database port is provided");
        options.UseNpgsql(
            $"Host={databaseConnectionString};Port={databasePort};Database=xtreamly;Username={databaseUser};Password={databasePassword}");
    }
    
});
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddIdentity<XtreamlyUser, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();


builder.Services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidAudience = builder.Configuration["JWT:ValidAudience"],
            ValidIssuer = builder.Configuration["JWT:ValidIssuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JWT:Secret"] ??
                                                                               throw new InvalidOperationException(
                                                                                   "no JWT:Secret in config")))
        };
    });

GlobalConfiguration.Configuration.UseSQLiteStorage();
builder.Services.AddHangfire(config =>
{
    config.SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UseSQLiteStorage();
});
builder.Services.AddHangfireServer(provider =>
{
    provider.WorkerCount = Environment.ProcessorCount * 5;
});


builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1",
        new OpenApiInfo()
        {
            Title = "Xtreamly API",
            Version = "v1"
        }
    );

    var filePath = Path.Combine(System.AppContext.BaseDirectory, "Xtreamly.xml");
    option.IncludeXmlComments(filePath);
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            Array.Empty<string>()
        }
    });
});
builder.Services.AddHealthChecks().AddDbContextCheck<ApplicationDbContext>();
var app = builder.Build();
app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(static origin => true) // allow any origin
    .AllowCredentials()); // allow credentials

app.MapHealthChecks("/health-check");




// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();



app.UseAuthorization();
app.MapControllers();

app.UseHangfireDashboard("/hangfire", new DashboardOptions
{
    Authorization = new [] { new HangfireAuthorizationFilter() }
});

await SetupDatabase();

async Task SetupDatabase()
{
    using var scope = app.Services.CreateScope();
    var dbContext = scope.ServiceProvider
        .GetRequiredService<ApplicationDbContext>();
    //bContext.Database.EnsureDeleted();
    dbContext.Database.EnsureCreated();
}
app.Run();